# Discord.js Tips
I'm hoping to give people some tips about bot making for Discord.

# Table of Contents
* [Generating Embeds](https://gitlab.com/yussuf.jpg/discordjs-tips/-/blob/master/en/Generating%20Embeds.md)
* [Basic Swear Detection](https://gitlab.com/yussuf.jpg/discordjs-tips/-/blob/master/en/Basic%20Swear%20Detection.md)
* [Logging User Based Activities](https://gitlab.com/yussuf.jpg/discordjs-tips/-/blob/master/en/Logging%20User%20Based%20Activities.md)

# Getting Started
Just find the folder that matches your language and begin.

# Getting The Most Out of This
[Go to the GitBooks page that i made!](http://yussufjpg.github.io/DiscordJS-Tips)

# Somethings That I Need to Say
I'm not an expert, I'm just a 16 year-old kid who wants to share his experiences, so there might be errors, grammar issues(My native language is Turkish), typos or outdated things etc.

>You can help me about this just by forking, fixing and sending as a pull request 👍

# Supported Languages
- [English](https://gitlab.com/yussuf.jpg/discordjs-tips/-/tree/master/en)
- [Türkçe](https://gitlab.com/yussuf.jpg/discordjs-tips/-/tree/master/tr)

# What's New
* Simple syntax mistakes fixed.
* Turkish language support! (Türkçe dil desteği!)
* README.md changes. (old one is in english version)
* The new file layout.
* Page linking, documentation support for logging pages.
* GitBooks Support!
